//5.0.-Remplazar el primero codigo que genera automatico
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';//5.5 mporta el DashboardComponent en el AppRoutingModule.
import { HeroDetailComponent } from './hero-detail/hero-detail.component';//5.9
const routes: Routes = [
  //La siguiente parte del archivo es donde configuras tus rutas. Las Rutas le indican al enrutador qué vista mostrar cuando un usuario hace clic en un enlace 
  { path: 'heroes', component: HeroesComponent },
  //5.6 Agrega una ruta al arreglo AppRoutingModule.routes que coincida con una ruta al DashboardComponent.
  { path: 'dashboard', component: DashboardComponent },
  //5.7 Agregar una ruta predeterminada Para que la aplicación navegue al dashboard automáticamente
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  //5.10 agrega una ruta parametrizada al arreglo de rutas que coincida con el patrón de ruta de la vista detalle del héroe.
  { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],// AppRoutingModule de importartaciones y lo configura con las rutas en un solo paso llamando RouterModule.forRoot():
  exports: [RouterModule]//AppRoutingModule exporta el RouterModule para que esté disponible en toda la aplicación
})
export class AppRoutingModule { }