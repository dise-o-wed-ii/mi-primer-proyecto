import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Título de la aplicación que se mostrará en la vista
  title = 'Recorrido de Super-Heroes Marvel';
}
