import { Component } from '@angular/core';
import { MessageService } from '../message.service'; // Importamos el servicio de mensajes desde el archivo message.service.ts
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent {
  //4.5Constructor que inyecta el servicio de mensajes para su uso en el componente
  constructor(public messageService: MessageService) {}
}
