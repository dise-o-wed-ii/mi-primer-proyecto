// Interfaz que define la estructura de un héroe
export interface Hero {
  id: number; // Propiedad para el ID del héroe
  name: string; // Propiedad para el nombre del héroe
  genero: string; // Propiedad para el género del héroe
}
