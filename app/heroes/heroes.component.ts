import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';//4.4.-Elimine la importación HEROES, porque ya no la necesitará. Importa el HeroService en su lugar.
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  // 3.1Propiedad para mantener un héroe seleccionado
  selectedHero!: Hero;
  //2.1 lista de heroes
  // 4.5.-Lista de héroes Reemplace la definición de la propiedad heroes con una simple declaración
  heroes!: Hero[];

  //4.6-4.17 Constructor que inyecta los servicios necesarios
  constructor(private heroService: HeroService, private messageService: MessageService) { }

  ngOnInit() {
    //4.7Llamamos a la función para obtener la lista de héroes al inicializar el componente
    this.getHeroes();
  }

  onSelect(hero: Hero): void {

    // 2.3Cuando se selecciona un héroe, actualizamos la propiedad selectedHero
    this.selectedHero = hero;
    //4.18 Añadimos un mensaje al servicio de mensajes para mostrar la selección del héroe
    this.messageService.add(`HeroesComponent: ID del héroe seleccionado =${hero.id}`);
  }

  getHeroes(): void {
   //4.8Llamamos al servicio para obtener la lista de héroes y suscribimos esta clase para recibir la respuest
    //4.8Llamamos al servicio para obtener la lista de héroes y suscribimos esta clase para recibir la respuest
    this.heroService['getHeroes']()
   .subscribe((heroes: Hero[]) => this.heroes = heroes);
  }
}