import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  //4.10 Array donde se almacenan los caracteres
  messages: string[] = [];

  //4.11 Método para agregar un mensaje a la lista de mensajes
  add(message: string) {
    this.messages.push(message);
  }

  //4.12 Método para limpiar la lista de mensajes
  clear() {
    this.messages = [];
  }
}
