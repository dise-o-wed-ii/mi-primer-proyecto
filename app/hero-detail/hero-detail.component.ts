import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: [ './hero-detail.component.css' ]
})
export class HeroDetailComponent implements OnInit {
  hero!: Hero; // Almacena el héroe actual.

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit(): void {
    // Se ejecuta al inicializar el componente.
    // Llama al método 'getHero' para obtener los detalles del héroe.
    this.getHero();
  }
  
  getHero(): void {
    // Obtiene el ID del héroe desde los parámetros de la URL y recupera los detalles del héroe.
    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam !== null) {
      const id = +idParam;
      this.heroService.getHero(id)
        .subscribe(hero => this.hero = hero!);
    }
  }

  //Esta forma de manejar errores es más robusta y facilita la gestión de situaciones en las que no se encuentra un héroe con el ID especificado. Además, proporciona una mejor separación de responsabilidades entre el servicio y el componente.  
  //La clave aquí es el operador filter, que se encarga de eliminar los valores indefinidos del flujo. Luego, se utiliza hero as Hero para afirmar que el valor es un objeto de tipo Hero.
  
  //Con estos cambios, deberías poder manejar adecuadamente el caso en el que no se encuentre un héroe con el ID especificado sin errores de tipo.

  goBack(): void {
    this.location.back();
  }
}