import { Hero } from './hero';

//2.0 Lista de héroes con sus propiedades: ID, nombre y género
export const HEROES: Hero[] = [
  { id: 11, name: 'Spider-Man', genero: 'Masculino' },
  { id: 12, name: 'Iron Man', genero: 'Masculino' },
  { id: 13, name: 'Thor', genero: 'Masculino' },
  { id: 14, name: 'Captain America', genero: 'Masculino' },
  { id: 15, name: 'Hulk', genero: 'Masculino' },
  { id: 16, name: 'Black Widow', genero: 'Femenino' },
  { id: 17, name: 'Black Panther', genero: 'Masculino' },
  { id: 18, name: 'Doctor Strange', genero: 'Masculino' },
  { id: 19, name: 'Ant-Man', genero: 'Masculino' },
  { id: 20, name: 'Wolverine', genero: 'Masculino' },
  { id: 21, name: 'Spider-Gwen', genero: 'Femenino' },
  { id: 22, name: 'Daredevil', genero: 'Masculino' },
  { id: 23, name: 'Scarlet Witch', genero: 'Femenino' },
  { id: 24, name: 'Hawkeye', genero: 'Masculino' },
  { id: 25, name: 'Vision', genero: 'Masculino' }
];
