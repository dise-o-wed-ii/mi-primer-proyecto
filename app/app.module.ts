import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// 1.2 Importamos el componente AppComponent desde el archivo app.component.ts
import { AppComponent } from './app.component'; 
// 2 Importamos el componente HeroesComponent desde el archivo heroes.component.ts
import { HeroesComponent } from './heroes/heroes.component'; 
import { FormsModule } from '@angular/forms';
// 3.-Importamos el componente HeroDetailComponent desde el archivo hero-detail.component.ts
import { HeroDetailComponent } from './hero-detail/hero-detail.component'; 
// 4.-Importamos el componente MessagesComponent desde el archivo messages.component.ts
import { MessagesComponent } from './messages/messages.component'; 
// 5.-Importamos el módulo de enrutamiento desde el archivo app-routing.module.ts
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component'; 

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
