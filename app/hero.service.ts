import { Injectable } from '@angular/core';
import { Hero } from './hero';//4.1.-importar Hero and HEROES.
import { HEROES } from './mock-heroes';//4.2.-Agregue el método getHeroes y devuelva los héroes simulados
import { Observable, of } from 'rxjs';//4.8.-importe los símbolos Observable y of de RxJS.
import { MessageService } from './message.service';//4.13.-En HeroService, importe el MessageService.

//4.0 (new service)
@Injectable({
  providedIn: 'root'
})
export class HeroService {
  [x: string]: any;
  //4.15 Agregue el método getHeroes y devuelva los héroes simulados.
  getHero(id: number): Observable<Hero | null> {
    this.messageService.add(`Heroes: heroe id=${id}`);
        // Busca un héroe por su ID en la lista de héroes simulados (HEROES).
    const hero = HEROES.find(hero => hero.id === id);
    return of(hero || null); // // Retorna el héroe si se encuentra, o null si no se encuentra.
  }
  getHeroes(): Observable<Hero[]> {
    // Agrega un mensaje indicando que se están obteniendo los héroes.
    this.messageService.add('HeroService: fetched heroes');
      // Retorna un Observable que emite la lista de héroes simulados (HEROES).
    return of(HEROES);
  }
  
  
  
  
  //4.14 constructor con un parámetro que declare una propiedad privada messageService
  constructor(private messageService: MessageService) { }
  
  
}
