import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero'; // Asegúrate de que Hero esté importado
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
// 5.7 Esta propiedad almacena la lista de héroes que se mostrará en la vista.

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    // 5.8 Este método se llama automáticamente cuando se inicializa el componente.
    // 5.8.1 Llama al método 'getHeroes' para cargar la lista de héroes al iniciar la vista.
    this.getHeroes();
  }
  

  getHeroes(): void {
    // 5.4 Llama al método 'getHeroes' del servicio y suscríbete a los héroes obtenidos
    this.heroService['getHeroes']()
      .subscribe((heroes: Hero[]) => this.heroes = heroes.slice(1, 5));
  }
  
}
